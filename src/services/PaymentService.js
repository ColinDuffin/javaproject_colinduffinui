import http from '../http-common';

export class PaymentService {
  status() {
    return http.get('/status');
  }

  savePayment(data) {
    return http.post('/save', data);
  }

  getAll() {
    var allPayments = http.get('/getallpayments');
    return allPayments;
  }

  getById(id) {
    return http.get(`/findbyid/${id}`);
  }
}

export default new PaymentService();
