import React from 'react';
import logo from './logo.svg';
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import { Switch, Route, Link } from 'react-router-dom';
import AddPaymentRedux from './components/AddPaymentRedux';
import ViewPaymentDetail from './components/ViewPaymentDetail';
import ViewPaymentsList from './components/ViewPaymentsList';

import About from './components/About';

function App() {
  return (
    <div id="container">
      <nav className="navbar navbar-expand navbar-dark bg-primary">
        <a href="/payments" className="navbar-brand">
          {/* Using anchor tag here, just to show different option from the Link
          tags below Home */}
          Payments
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={'/addpaymentredux'} className="nav-link">
              Add Payment
            </Link>
          </li>

          <li className="nav-item">
            <Link to={'/about'} className="nav-link">
              About
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Switch>
          <Route exact path={['/', '/payments']} component={ViewPaymentsList} />
          <Route
            exact
            path={['/', '/addpaymentredux']}
            component={AddPaymentRedux}
          />

          <Route path="/paymentdetail/:id" component={ViewPaymentDetail} />

          <Route exact path={['/', '/about']} component={About} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
