import React, { Component } from 'react';

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appNamm: 'Employee',
      year: '2020',
      apiStatus: 'superunknown',
    };
  }

  async componentDidMount() {
    var url = 'http://localhost:8080/api/status';
    let textData;
    try {
      let response = await fetch(url);
      textData = await response.text();
    } catch (e) {
      console.log('error ' + e.toString());
    }
    this.setState({ apiStatus: textData });
  }

  render() {
    return (
      <>
        <h1>API Status is {this.state.apiStatus}</h1>
      </>
    );
  }
}

export default About;
