import React, { Component } from 'react';
import { connect } from 'react-redux';
import PaymentList from '../redux/actions/PaymentActions';
import { Link } from 'react-router-dom';
import { Redirect } from 'react';

class ViewPlaymentsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: -1,
      paymentList: [],
    };
  }

  componentDidMount() {
    this.props.dispatch(PaymentList());
  }

  render() {
    const { payments, error, pending } = this.props;

    function leadingZeros(num, size) {
      var s = num + '';
      while (s.length < size) s = '0' + s;
      return s;
    }

    return (
      <>
        <div className="container">
          <div className="col-md-12">
            <h1>All Payments</h1>

            <ul className="list-group">
              <li className="list-group-item">
                <div className="row">
                  <div className="col-sm font-weight-bold text-center">
                    Payment ID
                  </div>
                  <div className="col-sm font-weight-bold text-center">
                    Customer ID
                  </div>
                  <div className="col-sm font-weight-bold text-center">
                    Payment Amount
                  </div>
                </div>
              </li>
              {payments &&
                payments.map((payment, index) => (
                  <li className="list-group-item" key={index}>
                    <div className="row">
                      <div className="col-sm text-center">
                        <Link to={`paymentdetail/${payment.id}`}>
                          {leadingZeros(payment.id, 4)}
                        </Link>
                      </div>
                      <div className="col-sm text-center">
                        {payment.custId}{' '}
                      </div>
                      <div className="col-sm text-center">
                        {new Intl.NumberFormat('en-GB', {
                          style: 'currency',
                          currency: 'GBP',
                        }).format(payment.amount)}
                      </div>
                    </div>
                  </li>
                ))}
            </ul>
          </div>
        </div>
      </>
    );
  }
}

const getPayments = (state) => state.paymentList;
const getPaymentsPending = (state) => state.pending;
const getPaymentsError = (state) => state.error;

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: () => dispatch(PaymentList()),
  };
};

const mapStateToProps = (state) => ({
  error: state.error,
  payments: state.payments,
  pending: state.pending,
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewPlaymentsList);
