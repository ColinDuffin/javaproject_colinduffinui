import React, { useState } from 'react';
import addPaymentAction from '../redux/actions/AddPaymentAction';
import { useDispatch } from 'react-redux';

function AddPaymentRedux(props) {
  const dispatch = useDispatch();
  const initialFormState = {
    custId: '',
    type: '',
    amount: '',
    submitted: false,
  };
  // const paymentOptions = [
  //   { value: 'Monthly', label: 'Monthly' },
  //   { value: 'Quarterly', label: 'Quarterly' },
  //   { value: 'Annual', label: 'Annual' },
  // ];
  const [payment, setPayment] = useState(initialFormState);
  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setPayment({ ...payment, [name]: value });
  };

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        dispatch(addPaymentAction(payment));
        window.location.href = 'http://localhost:3000';
      }}
    >
      <h1>Add Payment</h1>
      <div className="form-group">
        <label htmlFor="custId">Customer ID</label>
        <input
          type="text"
          className="form-control"
          id="custId"
          name="custId"
          aria-describedby="custIdHelp"
          placeholder="Customer ID"
          value={payment.custId}
          required
          onChange={handleInputChange}
        />
        <small id="custIdHelp" className="form-text text-muted">
          We'll never share your details with anyo else.
        </small>
      </div>
      <div className="form-group">
        <label htmlFor="type">Payment Type</label>
        <input
          type="text"
          className="form-control"
          id="type"
          name="type"
          aria-describedby="typeHelp"
          placeholder="Payment Type"
          value={payment.type}
          required
          onChange={handleInputChange}
        />
        <small id="typeHelp" className="form-text text-muted">
          We'll never share your details with anyone else.
        </small>
      </div>
      {/* <div className="form-group">
        <label htmlFor="type">Payment Type</label>
        <Select
          options={paymentOptions}
          id="type"
          name="type"
          aria-describedby="typeHelp"
          placeholder="Payment Type"
          value={payment.type}
          required
          onChange={handleInputChange}
        />
        <small id="typeHelp" className="form-text text-muted">
          We'll never share your details with anyone else.
        </small>
      </div> */}

      {/* <div className="form-group">
        <label htmlFor="amount">Payment Amount</label>
        <input
          type="email"
          className="form-control"
          id="email"
          name="email"
          aria-describedby="emailHelp"
          placeholder="Enter email"
          value={payment.email}
          required
          onChange={handleInputChange}
        />
        <small id="emailHelp" className="form-text text-muted">
          We'll never share your email with anyone else.
        </small>
      </div> */}
      <div className="form-group">
        <label htmlFor="amount">Payment Amount</label>
        <input
          type="number"
          className="form-control"
          id="amount"
          name="amount"
          aria-describedby="amountHelp"
          placeholder="Payment Amount"
          value={payment.amount}
          required
          onChange={handleInputChange}
        />
        <small id="amountHelp" className="form-text text-muted">
          We'll never share your salary with anyone else.
        </small>
      </div>
      {/* <div className="form-group">
        <label htmlFor="id">Id</label>
        <input
          type="number"
          className="form-control"
          id="id"
          name="id"
          aria-describedby="emailid"
          placeholder="Enter id"
          value={payment.id}
          onChange={handleInputChange}
        />
      </div> */}
      <div className="form-group">
        <input
          type="submit"
          className="form-control btn btn-secondary"
          value="Save"
        ></input>
      </div>
    </form>
  );
}

export default AddPaymentRedux;
