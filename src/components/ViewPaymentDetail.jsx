import React, { Component } from 'react';
import PaymentService from '../services/PaymentService';

class ViewPaymentDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentID: this.props.match.params.id,
      payment: { id: '', amount: '', custId: '', paymentDate: '', type: '' },
    };
  }

  componentDidMount() {
    this.retrievePayment(this.state.paymentID);
  }

  async retrievePayment(paymentID) {
    await PaymentService.getById(paymentID)
      .then((response) => {
        this.setState({
          payment: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { payment } = this.state;

    function leadingZeros(num, size) {
      var s = num + '';
      while (s.length < size) s = '0' + s;
      return s;
    }

    return (
      <>
        <h1>Payment Details</h1>

        <div className="container">
          <div className="row">
            <div className="col-sm font-weight-bold text-center">
              Payment ID
            </div>
            <div className="col-sm font-weight-bold text-center">
              Customer ID{' '}
            </div>
            <div className="col-sm font-weight-bold text-center">
              Payment Type
            </div>
            <div className="col-sm font-weight-bold text-center">
              Payment Amount
            </div>
            <div className="col-sm font-weight-bold text-center">
              Payment Date
            </div>
          </div>
          <div className="row">
            <div className="col-sm text-center ">
              {leadingZeros(payment.id, 4)}
            </div>
            <div className="col-sm text-center">{payment.custId} </div>
            <div className="col-sm text-center">{payment.type}</div>

            <div className="col-sm text-center">
              {new Intl.NumberFormat('en-GB', {
                style: 'currency',
                currency: 'GBP',
              }).format(payment.amount)}
            </div>
            <div className="col-sm text-center">
              {new Intl.DateTimeFormat('en-GB', {
                year: 'numeric',
                month: 'long',
                day: '2-digit',
              }).format(payment.payment)}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default ViewPaymentDetail;
