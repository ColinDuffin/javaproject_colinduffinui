import {
  PAYMENT_LIST_REQUEST,
  PAYMENT_LIST_SUCCESS,
  PAYMENT_LIST_FAILURE,
} from '../constants/PaymentConstants';

const paymentInitialState = {
  pending: false,
  payments: [],
  error: null,
};

function PaymentListReducer(state = paymentInitialState, action) {
  switch (action.type) {
    case PAYMENT_LIST_REQUEST:
      return { ...state, pending: true };
    case PAYMENT_LIST_SUCCESS:
      return { ...state, pending: false, payments: action.payload };
    case PAYMENT_LIST_FAILURE:
      return { ...state, pending: false, error: action.payload };

    default:
      return state;
  }
}

export default PaymentListReducer;
