import {
  PAYMENT_ADD_REQUEST,
  PAYMENT_ADD_SUCCESS,
  PAYMENT_ADD_FAILURE,
} from '../constants/PaymentConstants';

import PaymentService from '../../services/PaymentService';

const AddPayment = (paymentData) => (dispatch) => {
  try {
    dispatch({ type: PAYMENT_ADD_REQUEST, payload: paymentData });
    console.log('Add Payment Action looking for paymentData');
    console.log(paymentData);
    const data = PaymentService.savePayment(paymentData).then((response) => {
      dispatch({ type: PAYMENT_ADD_SUCCESS, payload: response.data });
    });
  } catch (error) {
    dispatch({ type: PAYMENT_ADD_FAILURE, payload: error });
  }
};

export default AddPayment;
