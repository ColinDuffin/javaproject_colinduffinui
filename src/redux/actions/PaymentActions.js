import {
  PAYMENT_LIST_REQUEST,
  PAYMENT_LIST_SUCCESS,
  PAYMENT_LIST_FAILURE,
} from '../constants/PaymentConstants';

import PaymentService from '../../services/PaymentService';

const PaymentList = () => (dispatch) => {
  try {
    dispatch({ type: PAYMENT_LIST_REQUEST });
    const data = PaymentService.getAll().then((response) => {
      dispatch({ type: PAYMENT_LIST_SUCCESS, payload: response.data });
    });
  } catch (error) {
    dispatch({ type: PAYMENT_LIST_FAILURE, payload: error });
  }
};

export default PaymentList;
