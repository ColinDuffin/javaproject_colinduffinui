export const PAYMENT_LIST_REQUEST = 'PAYMENT_LIST_REQUEST';
export const PAYMENT_LIST_SUCCESS = 'PAYMENT_LIST_SUCCESS';
export const PAYMENT_LIST_FAILURE = 'PAYMENT_LIST_FAILURE';

export const PAYMENT_ADD_REQUEST = 'PAYMENT_ADD_REQUEST';
export const PAYMENT_ADD_SUCCESS = 'PAYMENT_ADD_SUCCESS';
export const PAYMENT_ADD_FAILURE = 'PAYMENT_ADD_FAILURE';
