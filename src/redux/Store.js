import { applyMiddleware, createStore } from 'redux';
import PaymentReducer from './reducers/PaymentReducers';
import thunk from 'redux-thunk';

const initialState = {};
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
export const middlewares = [thunk];

const store = createStore(
  PaymentReducer,
  initialState,
  composeEnhancer(applyMiddleware(...middlewares))
);

export default store;
