import { connect } from 'react-redux';
import AddPaymentComponent from '../components/AddPayment';
import AddPaymentAction from '../actions/AddPaymentAction';

const mapStateToProps = (state) => {
  return {
    test: state,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    AddPaymentAction: () => dispatch(AddPaymentAction()),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddPaymentComponent);
